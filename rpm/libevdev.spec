Name:       libevdev
Summary:    libevdev is a library for handling evdev kernel devices
Version:    1.6.0
Release:    1
Group:      System/Libraries
License:    X11
URL:        http://www.freedesktop.org/software/libevdev/doc/latest/
Source0:    http://www.freedesktop.org/software/%{name}/%{name}-%{version}.tar.xz

Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig
BuildRequires:  doxygen
BuildRequires:  automake autoconf

%description
%{summary}

%package tools
Summary:    Tools for %{name}
Group:      System/Tools
Requires:   %{name} = %{version}-%{release}

%description tools
%{summary}.


%package devel
Summary:    Development headers/libraries for %{name}
Group:      Development/Libraries
Requires:   %{name} = %{version}-%{release}

%description devel
%{summary}.

%prep
%setup -q -n %{name}-%{version}/upstream

%build
./autogen.sh
%configure --disable-static
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
%make_install

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%{_libdir}/libevdev.so.*

%files tools
%{_bindir}/libevdev-tweak-device
%{_bindir}/mouse-dpi-tool
%{_bindir}/touchpad-edge-detector

%files devel
%defattr(-,root,root,-)
%{_includedir}/libevdev-1.0/libevdev/*.h
%{_libdir}/libevdev.so
%{_libdir}/pkgconfig/libevdev.pc
%doc %{_datadir}/man/man3/libevdev.3.gz
